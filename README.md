# Atmel AT86RF233
This is a basic port of the Arduino driver for the `Atmel AT86RF233` transceiver on the STM32 platform (tested on STM32F4xx family MCUs, but since it uses exclusively HAL functions for SPI communication, it should work on others too.)

This version is really early and more of a proof of concept (main.cpp implements a simple sniffer, listening for IEEE 802.15.4 data packets) but is usable enough for now.

### Source
The driver's source (if you want to use it separately) is included in the following files:

#### Inc/ (headers)
* `at86rf2xx.hpp`               _(class definition)_
* `at86rf2xx-defaults.hpp`      _(default values for setup, see datasheet)_
* `at86rf2xx-registers.hpp`     _(register addresses and masks)_
* `ieee802154.hpp`              _(IEEE 802.15.4 default values)_

#### Src/ (source code)
* `at86rf2xx.cpp`               _(implementation of the core functions)_
* `at86rf2xx-getset.cpp`        _(various getters and setters for transceiver control)_
* `at86rf2xx-internal.cpp`      _(internally-used functions such as register access)_

The rest of the files are mere scaffolding for the demo contained in main.cpp (in fact, a CubeMX project for STM32F4 modified to be compiled with a C++ compiler, whose only job is to initialize the SPI and UART peripherals for debugging purposes) so they can be thrown away safely.

### Usage
```cpp
#include "at86rf2xx.hpp" 
```
and instantiate an AT86RF2XX object. 

MAKE SURE THE APPROPRIATE INTERFACES ARE CORRECTLY INITIALIZED AND WORKING BEFORE ATTEMPTING
INSTANTIATION!
